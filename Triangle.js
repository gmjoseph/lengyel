class Triangle {
	constructor(p0, p1, p2) {
		Vec3._check(p0);
		Vec3._check(p1);
		Vec3._check(p2);
		this.p0 = p0;
		this.p1 = p1;
		this.p2 = p2;
	}

	static triangle(p0, p1, p2) {
		var tri = new Triangle(p0, p1, p2);
		return tri;
	}

	get normal() {
		if (!this._n) {
			var { p0, p1, p2 } = this;
			this._n = Vec3.cross(Vec3.subtract(p1, p0), 
				Vec3.subtract(p2, p0));
			this._n = Vec3.normalize(this._n);
		}
		return this._n;
	}

	get plane() {
		// p141
		// we'll leverage the plane here to see if a ray
		// intersects the triangle. then it's a matter of
		// seeing if the point of intersection falls within
		// the bounds of the triangle itself or not.
		if (!this._plane) {
			this._plane = Plane.plane(this.normal, this.p0);
		}
		return this._plane
	}

	rayIntersection(ray) {
		var p = this.plane.rayIntersection(ray);
		if (!p) {
			// plane's rayIntersection returns null if
			// there was no intersection
			return null;
		}
		// p141
		// use barycentric coordinates of the point of 
		// intersection p to see if that point is inside
		// the triangles edges.
		var { p0, p1, p2 } = this;
		var r = Vec3.subtract(p, p0);
		var q1 = Vec3.subtract(p1, p0);
		var q2 = Vec3.subtract(p2, p0);
		// R = w1 * q1 + w2 * q2, where w1 and w2 are
		// some values affecting the weighted average
		// of the triangles vertices.

		// p 142
		// shows how we can solve our w1 and w2 values
		var qMat = [
			[Vec3.dot(q1, q1), Vec3.dot(q1, q2)],
			[Vec3.dot(q1, q2), Vec3.dot(q2, q2)]
		];
		var qVec = [
			Vec3.dot(r, q1),
			Vec3.dot(r, q2)
		];

		var w12 = Vec2.multiplyMat2(qVec, Mat2.inverse(qMat));

		var liesWithinTriangle = w12[0] + w12[1] <= 1;
		if (!liesWithinTriangle) {
			return null;
		}

		var w2 = w12[1];
		var w1 = w12[0];
		var w0 = 1 - w1 - w2;

		var wp0 = Vec3.multiply(p0, w0);
		var wp1 = Vec3.multiply(p1, w1);
		var wp2 = Vec3.multiply(p2, w2);

		var wp = Vec3.add(wp0, wp1);
		wp = Vec3.add(wp, wp2);
		return wp;
	}
}