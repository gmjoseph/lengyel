class Ellipsoid {
	// p145
	// similar to the sphere, where the ellipsoid defines
	// a ratio of the x and y semiaxes to each other as m
	// and the x and z semiaxes as n. i.e. we're modifying
	// inputs to the equation by a ratio, allowing us to
	// still calculate a common = r^2 radius.
	constructor(radius, m, n) {
		this.radius = radius;
		this.m = m;
		this.n = n;
	}

	rayIntersection(ray) {
		var { radius, m, n } = this;
		var { s, v } = ray;

		var rr = radius * radius;
		var mm = m * m;
		var nn = n * n;
		var a = v[0] * v[0] + mm * v[1] * v[1] + nn * v[2] * v[2];
		var b = 2 * (s[0] * v[0] + mm * s[1] * v[1] + nn * s[2] * v[2]);
		var c = s[0] * s[0] + mm * s[1] * s[1] + nn * s[2] * s[2] - rr;

		var quadratic = new PolynomialQuadratic(a, b, c);
		var D = quadratic.discriminant;

		if (D < 0) {
			return null;
		} else if (D > 0 || 0 === D) {
			// d = 0 is the tangent case. we can treat it
			// like an intersection?
			// two points of intersection, on near and one far.
			// this gives us the near point of intersection,
			// rather than the far one. it's t[1] if we
			// access it off the PolynomialQuadratic.
			var t = (-b - Math.sqrt(D)) / (2 * a);
			var point = Vec3.add(s, Vec3.multiply(v, t));
			return point;
		}

		return null;
	}

	normal(point) {
		// p149
		// we know that a normal N comes from
		// the derivative of the implicit function:
		// 2x/a^2, 2y/b^2, 2z/c^2.
		Vec3._check(point);
		var x = point[0];
		var y = point[1];
		var z = point[2];
		var { radius, m, n } = this;
		// m and n are semi-axis ratios for x/y and x/z
		// respectively, so we need to convert them into
		// proper lengths, not fractions. since the radius
		// squared is on the right hand side of our 
		// parametric equation, we can divide by it and
		// work on removing m and n as coefficients of
		// y^2 and z^2.
		var a = radius;
		var b = m * radius;
		var c = n * radius;

		return [
			2 * x / (a * a),
			2 * y / (b * b),
			2 * z / (c * c)
		];
	}
}