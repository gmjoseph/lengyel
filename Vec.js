class Vec {
	static _check(v1, v2) {
		if (v1.length !== v2.length) {
			throw new Error(`mismatched vec length ${v1.length} ${v2.length}`);
		}
	}

	static equal(v1, v2) {
		Vec._check(v1, v2);
		for (var i = 0; i < v1.length; i++) {
			if (v1[i] !== v2[i]) {
				return false;
			}
		}
		return true;
	}

	static add(v1, v2) {
		Vec._check(v1, v2);
		return v1.map((a, i) => a + v2[i]);
	}

	static subtract(v1, v2) {
		Vec._check(v1, v2);
		return Vec.add(v1, Vec.multiply(v2, -1));
	}

	static multiply(v, scalar) {
		return v.map(a => a * scalar);
	}

	static dot(v1, v2) {
		Vec._check(v1, v2);
		return v1.reduce((
			(acc, a, i) => acc + a * v2[i]), 0);
	}

	static magnitude(v) {
		return Math.sqrt(v.reduce(
			(acc, a) => (acc + a * a), 0));
	}

	static angle(v1, v2) {
		Vec._check(v1, v2);
		// p15 solving for cos(a).
		var d = Vec.dot(v1, v2);
		var m1 = Vec.magnitude(v1);
		var m2 = Vec.magnitude(v2);
		return Math.acos(d / (m1 * m2));
	}

	static normalize(v) {
		var m = Vec.magnitude(v);
		if (0 === m) {
			return [0, 0, 0];
		}
		return Vec.multiply(v, 1 / Vec.magnitude(v));
	}

	static cross(v1, v2) {
		if (3 !== v1.length || 3 !== v2.length) {
			throw new Error(`cross product for 3d vectors only`);
		}
		// p21 cross product is perpendicular to
		// both vectors, so we can get the angle
		// of the new vector and an input vector
		// and it should be 90deg.
		return [
			v1[1] * v2[2] - v1[2] * v2[1],
			v1[2] * v2[0] - v1[0] * v2[2],
			v1[0] * v2[1] - v1[1] * v2[0]
		];
	}

	static projection(v1, v2) {
		// p29 the projection of v1 onto v2.
		// verify: // https://www.symbolab.com/solver/vector-projection-calculator/projection%20%5Cleft(1%2C%202%2C%203%5Cright)%2C%20%5Cleft(4%2C%203%2C%202%5Cright)
		var d = Vec.dot(v1, v2);
		var m = Vec.magnitude(v2);
		return Vec.multiply(v2, (d / (m * m)));
	}

	static area(v1, v2, v3) {
		// we're dealing with triangles here, not so much vectors.
		if (3 !== v1.length || 3 !== v2.length, 3 !== v3.length) {
			throw new Error(`area for 3d vectors only`);
		}
		// a = 1/2 * magnitude(cross((v2 - v1), (v3 - v1)))
		var c = Vec.cross(
			Vec.subtract(v2, v1), 
			Vec.subtract(v3, v1)
		);
		return 0.5 * Vec.magnitude(c);
	}
}