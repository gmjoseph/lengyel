class Cylinder {
	// p146
	// coordinate systems are switched around in the
	// book, where x is forward, y is right, and z
	// is up. we'll preserve the x-right, y-up, z-forward
	// system.
	// it also assumes the base of the cylinder is 
	// centred on the origin 0, 0, 0.
	// any point on the cylinder is decribed as
	// z^2 + m^2 * x^2 = r^2. remember s is represented
	// here as m, the ratio of r:s. and where
	// 0 <= y <= height.
	constructor(s, r, height) {
		this.s = s;
		this.r = r;
		this.height = height;
	}

	get m() {
		if (!this._m) {
			this._m = this.r/this.s;
		}
		return this._m;
	}

	rayIntersection(ray) {
		var { height, r, m } = this;
		var { s, v } = ray;

		var rr = r * r;
		var mm = m * m;

		// p146
		// as we said above, the y-axis is forward in the
		// diagram, whereas we know the z-axis is forward
		// in our case. it makes sense because s is on our
		// x-axis, and we have r, so we need to scale our 
		// x-axis values by m.
		var a = v[2] * v[2] + mm * v[0] * v[0];
		var b = 2 * (s[2] * v[2] + mm * s[0] * v[0]);
		var c = s[2] * s[2] + mm * s[0] * s[0] - rr;

		var quadratic = new PolynomialQuadratic(a, b, c);
		var D = quadratic.discriminant;

		if (D < 0) {
			return null;
		} else if (D > 0 || 0 === D) {
			// d = 0 is the tangent case. we can treat it
			// like an intersection?
			// two points of intersection, on near and one far.
			// this gives us the near point of intersection,
			// rather than the far one. it's t[1] if we
			// access it off the PolynomialQuadratic.
			var t = (-b - Math.sqrt(D)) / (2 * a);
			var point = Vec3.add(s, Vec3.multiply(v, t));

			// p146
			// we still need to do a height check for this point
			// since all we've established are values on the
			// infinite s and r planes (x and z in this case).
			if (point[1] < 0 || point[1] > height) {
				return null;
			}
			return point;
		}

		return null;
	}

	normal(point) {
		// y is irrelevant, we know any point that we hit
		// on the cylinder on it's circular contour is pointing
		// outward, except for the caps, which point upward
		// and downward ([0, 1, 0] and [0, -1, 0] respectively).
		//
		// one option is to start the normal at the centreline
		// and trace it through the point of intersection:
		// https://stackoverflow.com/questions/36266357/how-can-i-compute-normal-on-the-surface-of-a-cylinder
		//

		// TODO
		// WIP
		// https://math.stackexchange.com/questions/1827724/how-do-i-find-the-normal-vector-at-point-p-on-a-cylinder-x2y2-1

		Vec3._check(point);
		var x = point[0];
		var y = point[1];
		var z = point[2];
		var { s, r, height } = this;

		var a = s;
		var b = height;
		var c = r;

		if (height === y) {
			return [0, 1, 0];
		} else if (0 === y) {
			return [0, -1, 0];
		}
		
		return [
			2 * x / (a * a),
			// anything hitting the contour isn't going to point
			// up, only outward or sideways.
			0,
			// 2 * y / (b * b),
			2 * z / (c * c)
		];
	}
}