class Mat3 {
	// length can be a function on Mat3.
	static _check(m) {
		if ((m.length | m[0].length | 3) !== 3) {
			throw new Error(`mismatched mat3 length ${m.length}`);
		}
	}

	static _checkBoth(m1, m2) {
		Mat3._check(m1);
		Mat3._check(m2);
	}

	static add(m1, m2) {
		Mat3._checkBoth(m1, m2);
		return m1.map((v, i) => Vec3.add(v, m2[i]));
	}

	static subtract(m1, m2) {
		Mat3._checkBoth(m1, m2);
		return Mat3.add(m1, Mat3.multiplyScalar(m2, -1));
	}

	static multiply(m1, m2) {
		Mat3._checkBoth(m1, m2);
		return [0, 1, 2].map(i => (
			[
				m1[i][0] * m2[0][0] + 
				m1[i][1] * m2[1][0] +
				m1[i][2] * m2[2][0],
				m1[i][0] * m2[0][1] + 
				m1[i][1] * m2[1][1] +
				m1[i][2] * m2[2][1],
				m1[i][0] * m2[0][2] + 
				m1[i][1] * m2[1][2] +
				m1[i][2] * m2[2][2],
			]
		));
	}

	static multiplyScalar(m, scalar) {
		Mat3._check(m);
		return m.map(v => Vec3.multiply(v, scalar));
	}

	static inverse(m) {
		Mat3._check(m);
		const d = Mat3.determinant(m);
		if (0 === d) {
			return null;
		}
		var dm = [
			[
				m[1][1] * m[2][2] - m[1][2] * m[2][1],
				m[0][2] * m[2][1] - m[0][1] * m[2][2],
				m[0][1] * m[1][2] - m[0][2] * m[1][1],
			],
			[
				m[1][2] * m[2][0] - m[1][0] * m[2][2],
				m[0][0] * m[2][2] - m[0][2] * m[2][0],
				m[0][2] * m[1][0] - m[0][0] * m[1][2],
			],
			[
				m[1][0] * m[2][1] - m[1][1] * m[2][0],
				m[0][1] * m[2][0] - m[0][0] * m[2][1],
				m[0][0] * m[1][1] - m[0][1] * m[1][0],
			],
		]
		return Mat3.multiplyScalar(dm, 1 / d);
	}

	static determinant(m) {
		Mat3._check(m);
		return (
			m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
			m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) +
			m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0])
		);
	}

	static transpose(m) {
		Mat3._check(m);
		return [
			[
				m[0][0], m[1][0], m[2][0]
			],
			[
				m[0][1], m[1][1], m[2][1]
			],
			[
				m[0][2], m[1][2], m[2][2]
			]
		];
	}

	static equal(m1, m2) {
		Mat3._checkBoth(m1, m2);
		for (var i = 0; i < m1.length; i++) {
			if (!(Vec3.equal(m1[i], m2[i]))) {
				return false;
			}
		}
		return true;
	}

	static orthogonal(m) {
		Mat3._check(m);
		// p68 a 3x3 orthogonal matrix is a matrix 
		// whose inverse is equal to its transpose
		return Mat3.equal(Mat3.inverse(m), Mat3.transpose(m));
	}
}