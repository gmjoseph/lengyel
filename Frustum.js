class Frustum {
	// frustum:
	// 		is a volume of space containing everything that 
	// is in a 3D scene. it's shaped like a pyramd where the
	// camera is positioned at the tip and looks lengthward
	// along the pyramid: 
	// o<|, the camera would be 'o', in this case.
	// and | would be the base, or end, of the frustum.
	// shortly after the camera, towards the end, is another
	// plane. this second plane is everything that the camera
	// can see within the frustum, aka the screen.
	// so a frustum is made up of 6 planes:
	// left, right, bottom, top are the edges of the screen.
	// near, the minimum distance that objects are visible
	// to the camera, anything between the near plane and the
	// camera won't be visible. it's equivalent to our screen
	// or our projection plane (the scene is projected onto it)
	// far, the maximum distance that objects are visible.
	// anything beyond the far plane isn't visible to our camera
	// ultimately, for something to be visible to the camera
	// it needs to lie within the 6 planes.

	// coordinate system:
	// in opengl, the camera looks at -z, y is up, and +x is 
	// right. this gives us a right-handed coordinate system.

	// fov:
	// this determines how far our camera is from the projection
	// plane (the near plane). we can call this value 'e'.
	// e = 1 / tan(a/2), where a is our horizontal field of
	// view angle.
	// 		we can think of this like a triangle where the base
	// has a length of 2 (from x = -1, to x = 1), and the height
	// is e, and a is the angle that's opposite from the base
	// and is representing by 'a'.
	// larger fovs are equivalent to shorter focal lengths.
	// we can make the camera 'zoom in' by reducing the fov
	// angle 'a' (in turn causing a longer focal length).
	// if we reduce a, we can bring the camera closer.
	// for example, if a = 60deg, e is 1.73. if a = 30deg
	// then e is 3.73 (reduced angle, greater e, greater
	// focal length).
	// 		the reason this causes a 'zoom in' is because 
	// the projection plane has been pushed farther away from
	// the camera, but closer to the objects in the scene
	// so if a sphere was 2 units away from a 60deg-based
	// projection plane, with e of 1.73, and we dropped it 
	// to 30deg, the sphere is still in the same place it was,
	// but the plane has now moved to 3.73, so it's sitting
	// right at the sphere. as a result the sphere looks
	// closer, essentially equivalent to zooming in, since
	// the object appear on the projection plane.
	// we also have a vertical field of view to consider.
	// since most displays are rectangular, we have a non 1
	// aspect ratio (1:1). e.g. 640x480 has an asepct ratio
	// of 0.75
	// 		the bottom and top planes intersect the near 
	// (projection) plane at y = +/-a, where a is the aspect 
	// ratio. this gives us our vertical field of view angle, 
	// B, which should be different from our horizontal one.
	// B = 2tan^-1(a/e)

	// in sum, our top and bottom frustum planes are at y = +/- a,
	// our left and right frustum planes are at x = +/- 1,
	// our near plane is 'e' from the camera, and the far plane
	// is at some distance from the near (projection) plane.

	static frustum(cameraPosition, aspectRatio, fovAngle, nearPlane, farPlane) {

		Vec3._check(cameraPosition);

		var f = new Frustum();
		f.fovAngle = fovAngle;
		f.cameraPosition = cameraPosition;
		f.aspectRatio = aspectRatio;
		// camera looks at -z by convention.
		f.nearPlane = nearPlane;
		f.farPlane = farPlane;
		return f;
	}

	// at some point it probably doesn't make sense to work
	// in terms of 'e' when we'll be dealing entirely with
	// the near and far planes.
	get e() {
		return 1 / Math.tan(this.fovAngle / 2);
	}

	get fovAngleVertical() {
		return 2 * Math.atan(this.aspectRatio / this.e);
	}

	// p107
	// frustum planes as plane equations of a normal
	// and their D component. this applies to opengl
	// which keeps the near plane at -1 and the far
	// plane at 1, and then scales the other 4 planes
	// accordingly.
	// a lot of this information might apply to deprecated
	// concepts in opengl, like glFrustum which isn't
	// used because we need to implement our own
	// projection matrix. that is, we aren't ever going
	// to rely on opengl's definition of a frustum.

	// p111
	// to display a 3d scene on a 2d surface, we need
	// to determine where each vertex in the 3d scene
	// needs to be drawn on the 2d space.
	// we can figure out where a vertex at P falls
	// on the projection plane by casting a ray from
	// the camera (at the origin) towards P that 
	// intersects it. the x and y (on the projection
	// plane) of the projected point are given by
	// x = -e/pz * px, y = -e/pz * py.
	// pz is negative because the camera points 
	// into -z.
	// recall, e is the distance from our camera
	// to the projection plane. also, if we did
	// this to 'z' we'd get z = -e/pz * pz, so
	// -e is always z in this case.
	projectVertex(vertex) {
		Vec3._check(vertex);
		var x = -this.e / vertex[2] * vertex[0];
		var y = -this.e / vertex[2] * vertex[1];
		return [x, y];
	}

	// p112
	// refer to the book if this part is getting complicated:
	// 		perspective projections try and map the view frustum
	// to a cube. so we're trying to map the projection plane
	// onto one face of a cube, mapping the top and bottom
	// planes onto the other two cube faces, and finally
	// mapping the far plane onto another cube face.
	// 		"This cube is the projection into 3D space of what is 
	// called homogeneous clip space"
	// it extends from -1 to +1 on x, y, and z in opengl
	// and the cube is centred on 0, 0, 0.
	// a 4x4 projection matrix lets us map the frustum
	// onto homogeneous clip space (the cube).
	// 		this 4x4 projection matrix "among other actions, 
	// places the negative z coordinate of a camera-space 
	// point into the w coordinate of the transformed point"
	// dividing everything by that w (aka our -z from camera
	// or frustum space) makes a 3d point that has 'normalized
	// device coordinates'.
	// 		long story short, multiypling our 3d vertices by 
	// the projection matrix creates other 3d vertices that are
	// normalized device coordinates. we then still need to
	// map those ndc's onto a 2d plane, after all.
	// we need to add the 'w' to our 3d vertex, so we set it
	// to 1, which indicates that the R3 vector is a position.
	// that also makes sense because our vertices are positions
	// in 3d space. 
	// 		if i understand right, we're ultimately getting 
	// these coordinates setup with perspective projection 
	// before they're ultimately rasterized in the shaders, 
	// based on the p111 discussion going into projection.
	//		also, the whole reason behind doing this multiplication
	// is because the simplified operation on p111 will always
	// result in z = -e (so z is always at the near plane). so
	// even if the original vertices had z at different 
	// locations, after projecting them onto the plane, we 
	// don't know which vertices are -truly- in front of
	// one another. the projection matrix multiplication
	// lets us preserve that information and still map correctly
	// onto the near (projection) plane.
	//		one last thing, the remapping of the z coordinates
	// is non-linear. this makes sense when thinking about the 
	// perceived change of an object.
	// 		if an object is at 1cm away from the eye and it 
	// moves 10cm, then a very large change is perceived. 
	// however, if the object is 1000cm away from the eye and 
	// it still moves 10cm, the perceived change isn’t as large.
	// in sum, an object that is closer to the viewer and moves 
	// has a larger perceived change than one farther away.

	// p113
	// 		our near (projection) plane sits at 'n' in the 
	// following equations. what's changed is we now have new
	// ways to map x, y and z to fit the view frustum into
	// the homogeneous clip space. x and y still take linear
	// functions, but z is more complicated. the reason for this
	// is because x and y are being mapped from 1 to -1 on their
	// respective axes. z is also being mapped from 1 to -1 
	// but it's source is based on the z position between the
	// near and far clip planes.
	// "We wish to find a function that maps −n → −1 and − f → 1."
	// where n is near and f is far.
	// here are our equations for each point:
	// x' = 2n / (r - l) * (-px / pz) - (r + l) / (r - l)
	// y' = 2n / (t - b) * (-py / pz) - (t + b) / (t - b)
	// z' = 2nf / (f - n) * (-1 / pz) + (f + n) / (f - n)
	// where n = near plane, f = far plane, 
	// l r t b = left, right, top, bottom, and are no doubt
	// all scalar values. they represent the edges of the near
	// plane. so, we can get l r t and b using trig because we
	// have the angle from the fov, and we've got the distance
	// from our camera to the near plane.
	// so for l, for example, we have horizontalFOV/2 as our
	// angle, e is our adjacent edge, and o is our opposite
	// unknown going towards l, so e * tan(horizontalFOV/2) = o

	get top() {
		// we might not need to scale by the aspect
		// ratio because our image is already going to
		// be mapped onto a plane that has a non 1:1 ratio..?
		return this.left / this.aspectRatio;
	}

	get bottom() {
		return -this.top;
	}

	get left() {
		// probably needs to be the nearplane instead of e.
		// why? because right now this.e is the complete
		// inverse of what's happening here, other than
		// the change in sign.
		// that is, this.e * Math.tan(this.fovAngle/2)
		// is always going to be 1! so we might as well
		// just set left to -1, but that's probably wrong.
		// return -Math.tan(this.fovAngle / 2) * this.e;

		// from p106.
		// return -this.nearPlane/this.e;
		return Math.tan(this.fovAngle * 0.5) * -this.nearPlane;
	}

	get right() {
		return -this.left;
	}

	get far() {
		return this.farPlane;
	}

	get near() {
		return this.nearPlane;
	}

	// p107
	// the normals of the planes bounding our view frustum,
	// based off of 'e' and the aspect ratio 'a'.
	get leftNormal4() { 
		var e = this.e;
		return [
			e/Math.sqrt(e * e + 1),
			0,
			-1/Math.sqrt(e * e + 1),
			0,
		];
	}
	get rightNormal4() { 
		var e = this.e;
		return [
			-e/Math.sqrt(e * e + 1),
			0,
			-1/Math.sqrt(e * e + 1),
			0,
		];
	}
	get topNormal4() { 
		var e = this.e;
		var a = this.aspectRatio;
		return [
			e/Math.sqrt(e * e + a * a),
			0,
			-a/Math.sqrt(e * e + a * a),
			0,
		];
	}
	get bottomNormal4() { 
		var e = this.e;
		var a = this.aspectRatio;
		return [
			-e/Math.sqrt(e * e + a * a),
			0,
			-a/Math.sqrt(e * e + a * a),
			0,
		];
	}
	// p114
	// our R3 point P' made of x', y', and z' is the same as
	// the R4 point P' made of (-x'/pz, -y'/pz, -z'/pz, -pz)
	//
	// 		finally, we can use these values to help us make
	// a 4x4 matrix to create that P' based on P with a w 
	// value of 1:
	// [
	// 	[2 * n / (r - l), 0, (r + l) / (r - l), 0],
	// 	[0, 2 * n / (t - b), (t + b) / (t - b), 0],
	//  [0, 0, -(f + n) / (f - n), -2 * n * f / (f - n), 0],
	//  [0, 0, -1, 0]
	// ]
	// we multiply that by [px, py, pz, 1].
	// another interesting resource:
	// https://www.scratchapixel.com/lessons/3d-basic-rendering/perspective-and-orthographic-projection-matrix/projection-matrix-introduction
	// 		that resource gives us some further insight. first,
	// the opengl perspective matrix presented is going to be 
	// __COLUMN-MAJOR__. so that matrix above should be transposed!
	//		second, we've kind of touched on how to calculate values
	// for r, l, t, b and n. the site goes into more details on each
	// of those. i still used lengyel's approach based on the opengl
	// frustum to get those values, but some of the commented
	// out code uses scratchapixel's approach.

	// p116
	// discusses the idea of being able to project points lying
	// at infinity. this'll be required for shadow-rendering
	// techniques used later on but can be skipped for now.
	// suffice it to say, that it's doable by slightly
	// reconfiguring the values in the projection matrix above
	perspectiveMatrix() {
		var n = this.near;
		var f = this.far;
		var r = this.right;
		var l = this.left;
		var t = this.top;
		var b = this.bottom;
		return [
			[2 * n / (r - l), 0, 0, 0],
			[0, 2 * n / (t - b), 0, 0],
			[(r + l) / (r - l), (t + b) / (t - b), -(f + n) / (f - n), -1],
			[0, 0, -2 * f * n / (f - n), 0],
		];
	}

	// p117
	// 		orthographic projection involves linear scaling 
	// along all 3 axes.
	// x' = 2 / (r - l) * x - (r + l) / (r - l)
	// y' = 2 / (t - b) * y - (t + b) / (t - b)
	// z' = -2 / (f - n) * z + (f + n) / (f - n)
	// 		notice the '2' everywhere? it's the distance 
	// between 1 and -1.
	// 		in matrix form we get:
	// [
	//	[2 / (r - l), 0, 0, -(r + l) / (r - l)],
	// 	[0, 2 / (t - b), 0, -(t + b) / (t - b)],
	//  [0, 0, -2 / (f - n), -(f + n) / (f - n)],
	//  [0, 0, 0, 1]
	// ]
	// multiplied by [px, py, pz, 1].
	// once again this is __COLUMN-MAJOR__ so we'd like to
	// transpose it.

	orthographicMatrix() {
		var n = this.near;
		var f = this.far;
		var r = this.right;
		var l = this.left;
		var t = this.top;
		var b = this.bottom;
		return [
			[2 / (r - l), 0, 0, 0],
			[0, 2 / (t - b), 0, 0],
			[0, 0, -2 / (f - n), 0],
			[-(r + l) / (r - l), -(t + b) / (t - b), -(f + n) / (f - n), 1],
		];
	}

	projectVertexPerspective(vertex) {
		Vec3._check(vertex);
		// convert to raster space next
		var p = this.projectVertexMat4(vertex, this.perspectiveMatrix());
		p[0] /= p[3];
		p[1] /= p[3];
		p[2] /= p[3];
		// p112
		// unlike the camera projection, we use n instead of e.
		var n = this.near;
		var x = -n / p[2] * p[0];
		var y = -n / p[2] * p[1];
		return [x, y];
	}

	projectVertexOrthographic(vertex) {
		Vec3._check(vertex);
		// convert to raster space next, unlike the
		// perspective projection matrix, it doesn't
		// seem to make sense to divide by the 'w'.
		// the reasoning here is that the 'w' scales
		// the x, y, and z coordinates, giving us
		// scaled up/down x, y and z positions of 
		// vertices as z is farther away from the 
		// camera. scaling makes no sense here since
		// we're just moving x and y along the z into
		// the near plane. that is, we're just fitting
		// those coords within the frustum, and sticking
		// them to the near plane.
		// p117 shows how this happens, with the vertices
		// moving towards our 'n' value.
		return this.projectVertexMat4(vertex, this.orthographicMatrix());
	}

	projectVertexMat4(vec3, mat4) {
		// going to vec4, we'll set vec3[3] to 1.
		return [
			vec3[0] * mat4[0][0] + vec3[1] * mat4[0][1] + vec3[2] * mat4[0][2] + 1 * mat4[0][3],
			vec3[0] * mat4[1][0] + vec3[1] * mat4[1][1] + vec3[2] * mat4[1][2] + 1 * mat4[1][3],
			vec3[0] * mat4[2][0] + vec3[1] * mat4[2][1] + vec3[2] * mat4[2][2] + 1 * mat4[2][3],
			vec3[0] * mat4[3][0] + vec3[1] * mat4[3][1] + vec3[2] * mat4[3][2] + 1 * mat4[3][3],
		];
	}

	// p122
	// to handle clipping objects at points of reflection:
	// "we look for a way to modify the projection matrix
	// so that the conventional near plane is repositioned
	// to coincide with the reflective surface, which is
	// generally oblique to the ordinary view frustum."
	obliqueClippingPerspectiveMatrix(m) {
		// WIP.
		throw new Error("converting perspective matrix to an oblique clipping perspective matrix isn't implemented yet");
	}
}