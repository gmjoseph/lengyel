class PolynomialQuadratic {
	// p132
	// most of these equations come from the quadratic
	// formula, and other shortcuts presented on that
	// page.
	constructor(a, b, c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	// "evaluating the discriminant allows us to determine
	// whether a ray intersects an object without actually
	// calculating the points of intersection."
	// where our ray is defined as p(t) = S + t * V.
	// notice how 't' carries over to a lot of places in
	// the quadratic polynomial.
	get discriminant() {
		var a = this.a;
		var b = this.b;
		var c = this.c;
		return (b * b) - (4 * a * c);
	}

	// the number of roots this quadratic has.
	get realRootsCount() {
		var d = this.discriminant;
		if (d > 0) {
			return 2;
		} else if (0 === d) {
			return 1;
		} else if (d < 0) {
			return 0
		}
	}

	get t() {
		if (!this._t) {
			var rrc = this.realRootsCount;
			if (0 === rrc) {
				// two imaginary roots.
				this._t = null;
			} else if (1 === rrc) {
				// if there is 1 real root, it can be
				// calculated like this.
				this._t = -this.b/this.a * 2;
			} else {
				var { a, b, c } = this;
				var d = this.discriminant;

				var t0 = (-b + Math.sqrt(d)) / 2 * a;
				var t1 = (-b - Math.sqrt(d)) / 2 * a;
				this._t = [t0, t1];
			}
		}
		return this._t;
	}
}