class Line {
	// p93
	// 2 points p1, p2, or 2 Vec3's

	static ray(s, v) {
		// p93
		// s = start point, v = direction vector.
		// p(t) = s + t(v) where t is >= 0.
		// can also represent lines
		Vec3._checkBoth(s, v);
		var l = new Line();
		l.s = s;
		l.v = v;
		return l;
	}

	static distancePointLine(point, line) {
		// project the point onto our line using 
		// the projection of the point onto the vector
		// that describes our line.
		// then find the distance of the point to the
		// projected point.
		// https://math.stackexchange.com/questions/1905533/find-perpendicular-distance-from-point-to-line-in-3d
		Vec3._check(point);
		var q = point;
		var d =  Vec.magnitude(
			Vec.subtract(
				Vec.projection(q, Vec.subtract(line.v, line.s)),
				q
			)
		);
		return d;
	}

	distancePoint(point) {
		return Line.distancePointLine(point, this);
	}

	static distanceLineLine(l1, l2, t1, t2) {
		// p95
		// this isn't giving us the -minimum- distance obviously
		// between the two skew lines. we'd need to somehow
		// pick the right t1, t2 on the lines to get that.
		// if (t1 < 0 || t1 > 1 || t2 < 0 || t2 > 1) {
		// 	throw new Error(`distance along line must be between 0 and 1`);
		// }
		var p1 = Vec3.add(l1.s, Vec.multiply(l1.v, t1));
		var p2 = Vec3.add(l2.s, Vec.multiply(l2.v, t2));
		return Vec3.magnitude(Vec.subtract(p2, p1));
	}

	static minimumDistanceLineLine(l1, l2) {
		// p95
		// we need to find t1, t2 in this case and then
		// minimize the magnitude of p1(t1) - p1(t2) aka
		// when we plug in the t1 and t2 into our 
		// parametric line to get a point, what's the
		// smallest magnitude we can get?
		
		// p96
		// verify: http://www.ambrsoft.com/TrigoCalc/Line3D/Distance2Lines3D_.htm
		// approach: http://eguruchela.com/math/Calculator/shortest-distance-between-lines
		// strangely, this approach works way more nicely
		// than what's presented in the book...

		var x = Vec3.cross(l1.v, l2.v);
		var y = Vec3.subtract(l2.s, l1.s);
		var z = Vec3.dot(x, y);

		var m = Vec3.magnitude(x);

		if (m !== 0) {
			return z / m;
		} else {
			return (Vec3.dot(l1.v, y)/ Vec3.magnitude(l1.v))
		}


		// var m1 = Vec3.magnitude(Vec3.subtract(l1.v, l1.s));
		// var m2 = Vec3.magnitude(Vec3.subtract(l2.v, l2.s)); 
		// var m1 = Vec3.magnitude(l1.v);
		// var m2 = Vec3.magnitude(l2.v); 

		// var mat2 = Mat2.inverse([
		// 	[
		// 		m1 * m1, 
		// 		Vec3.dot(Vec3.multiply(l1.v, -1), l2.v)
		// 	],
		// 	[
		// 		Vec3.dot(l1.v, l2.v), 
		// 		-m2 * m2
		// 	]
		// ]);

		// var v2 = [
		// 	Vec3.dot(Vec3.subtract(l2.s, l1.s), l1.v),
		// 	Vec3.dot(Vec3.subtract(l2.s, l1.s), l2.v),
		// ];

		// var t = Vec2.multiplyMat2(v2, mat2);

		// // now we've got t so we can plug the t values
		// // back into our parametric line equations to
		// // find the distance.
		// var d = Line.distanceLineLine(l1, l2, t[0], t[1]);
		// return Math.sqrt(d);
	}
}