class PolynomialQuartic {
	constructor(a, b, c, d, e) {
		// we have 5 values, but a good shortcut is to always
		// let a = 1, so we can skip all the operations on a.
		// also use this to get our p, q, r values. 
		// https://math.stackexchange.com/a/57688 also has a 
		// couple of nice solutions and explanations
		this.a = b/a;
		this.b = c/a;
		this.c = d/a;
		this.d = e/a;
	}

	get p() {
		if (!this._p) {
			var { a, b } = this;
			var x = -3 / 8;
			var y = a * a;
			var z = x * y;
			this._p = z + b;
		}
		return this._p;
	}

	get q() {
		if (!this._q) {
			var { a, b, c } = this;
			var w = 1 / 8;
			var x = a * a * a;
			var y = 1 / 2;
			var z = a * b;
			this._q = w * x - y * z + c;
		}
		return this._q;
	}

	get r() {
		if (!this._r) {
			var { a, b, c, d } = this;
			var u = -3 / 256;
			var v = a * a * a * a;
			var w = 1 / 16;
			var x = a * a * b;
			var y = 1 / 4;
			var z = a * c;
			this._r = u * v + w * x - y * z + d;
		}
		return this._r;
	}

	get depressed() {
		if (!this._depressed) {
			var { p, q, r } = this;
			this._depressed = new PolynomialQuartic(1, 0, p, q, r);
		}
		return this._depressed;
	}

	get y() {
		if (!this._y) {
			// p136
			// y is one possible solution to a cubic polynomial 
			// then lets us find solutions to the depressed quartic 
			// based on whether this.q >= 0 or this.q < 0.
			// the coefficients for that cubic are made up of
			// the values we get for p, r and q.
			var { p, q, r } = this;
			
			var f0 = (-p / 2);
			var f1 = -r;

			var x = 4 * r * p;
			var y = q * q;

			var f2 = (x - y) / 8;

			var cubic = new PolynomialCubic(1, f0, f1, f2);

			// here's our 'any real solution' to the cubic
			// that was generated from our depressed quartic.
			var y = cubic.t;
			this._y = y.length ? y[0] : y;
		}
		return this._y;
	}


	t0() {
		// p136
		// depending on our q value, we get solutions for
		// the depressed quartic from two qudratic equations.
		var { p, r, y } = this;

		var yyp = 2 * y - p;
		var sqrtyyp = Math.sqrt(yyp);

		var y2r = y * y - r;
		var sqrty2r = Math.sqrt(y2r);

		var q0 = new PolynomialQuadratic(1, sqrtyyp, (y - sqrty2r));
		var q1 = new PolynomialQuadratic(1, -sqrtyyp, (y + sqrty2r));

		var t = [];
		if (q0.t) {
			if (q0.t.length) {
				t = t.concat(q0.t);
			} else {
				t.push(q0.t);
			}
		}
		if (q1.t) {
			if (q1.t.length) {
				t = t.concat(q1.t);
			} else {
				t.push(q1.t);
			}
		}
		return t;
	}

	t1() {
		// p136
		// depending on our q value, we get solutions for
		// the depressed quartic from two qudratic equations.
		var { p, r, y } = this;

		var yyp = 2 * y - p;
		var sqrtyyp = Math.sqrt(yyp);

		var y2r = y * y - r;
		var sqrty2r = Math.sqrt(y2r);

		var q0 = new PolynomialQuadratic(1, sqrtyyp, (y + sqrty2r));
		var q1 = new PolynomialQuadratic(1, -sqrtyyp, (y - sqrty2r));

		var t = [];
		if (q0.t) {
			if (q0.t.length) {
				t = t.concat(q0.t);
			} else {
				t.push(q0.t);
			}
		}
		if (q1.t) {
			if (q1.t.length) {
				t = t.concat(q1.t);
			} else {
				t.push(q1.t);
			}
		}
		return t;
	}

	get t() {
		// a depressd doesn't need to make another depressed.
		// and in this case a is really the coefficeint 
		// of the cubed term (x^3) because we divided everything
		// by a.
		if (!this._t) {
			var { a } = this;
			if (0 === a) {
				// we need a solution for the depressed to return
				// back up to the original quartic.
				this._t = this.q >= 0 ? this.t0() : this.t1();
			} else {
				this._t = this.depressed.t;
				for (var i = 0; i < this._t.length; i++) {
					this._t[i] -= (a / 4);
				}
			}
		}
		// an empty array means no solutions.
		return this._t;
	}

	test() {
		var { a, b, c, d } = this;
		var solutions = this.t;

		if (!solutions.length) {
			solutions = [solutions];
		}

		var passed = true;
		for (var i = 0; i < solutions.length; i++) {
			var t = solutions[i];
			var v = t * t * t * t * 1
			var w = t * t * t 	  * a;
			var x = t * t		  * b;
			var y = t 		  	  * c;
			var z = 1 		  	  * d;
			var f = v + w + x + y + z;
			if (0 !== Math.round(f)) {
				passed = false;
				break;
			}
		}

		return `test passed: ${passed}`;
	}
}