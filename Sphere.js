class Sphere {
	// p144
	// like the box, we're assuming things relative to the origin
	// so we just need a radius for our sphere because we know it's
	// at 0, 0, 0.
	constructor(radius) {
		this.radius = radius;
	}

	rayIntersection(ray) {
		// p144
		// solving this involves finding roots for our quadratic
		// the reason is, we can substitue the ray's components
		// into the sphere's equation: x^2 + y^2 + z^2 = r^2.
		// r is known, it's the radius.
		// if the ray intersects with our sphere, there is a value
		// t where s + t*v produces a vector where the vector's
		// x, y and z components squared do indeed equal r^2.

		var r = this.radius;
		var { s, v } = ray;

		var a = Vec3.dot(v, v);
		var b = Vec3.dot(s, v) * 2;
		var c = Vec.dot(s, s) - r * r;

		var quadratic = new PolynomialQuadratic(a, b, c);
		var D = quadratic.discriminant;

		if (D < 0) {
			return null;
		} else if (D > 0 || 0 === D) {
			// d = 0 is the tangent case. we can treat it
			// like an intersection?
			// two points of intersection, on near and one far.
			// this gives us the near point of intersection,
			// rather than the far one. it's t[1] if we
			// access it off the PolynomialQuadratic.
			var t = (-b - Math.sqrt(D)) / (2 * a);
			var point = Vec3.add(s, Vec3.multiply(v, t));
			return point;
		}

		return null;
	}
}