
class PolynomialNewton {
	// the idea is to support an arbitrary order.
	constructor(elements, maxIterations = 10) {
		// left to right: e.g. 2x^2 + x + 1 =
		// 2, 1, 1, where the last element has no 
		// variable.
		this.cs = elements;

		// generate the powers
		this.powers = this.cs.map(function(d, i, arr) {
			return arr.length - i - 1;
		});

		this.maxIterations = maxIterations;
	}

	get derivative() {
		// left to right: e.g. 2x^2 + x + 1 =
		// 2, 1, 1 from our elements.
		// since there are 3 terms, and 2 exponents...
		// exp - 1 * 2, exp - 1 * 1, etc. to get our
		// new polynomial.
		if (!this._derivative) {
			var nextElements = [];
			// var nextPowers = [];
			var currentPowers = this.powers;
			this.cs.forEach(function(d, i, arr) {
				var power = currentPowers[i];
				var nextElement = d * power;
				var nextPower = power - 1;
				if (nextPower < 0) {
					return;
				}
				nextElements.push(nextElement);
				// nextPowers.push(nextPower);
			});
			if (nextElements.length !== this.cs.length - 1) {
				throw new Error("whoops.");
			}
			this._derivative = new PolynomialNewton(nextElements);
		}
		return this._derivative;
	}

	f(x) {
		var y = 0;
		var coefficients = this.cs;
		this.powers.forEach(function(p, i) {
			var c = coefficients[i];
			y += (c * Math.pow(x, p));
		});
		return y;
	}

	guess(x) {


		// plug x0 back into our equation.
		// our only unknown here is x.
		// y - f(x0) = derivative(x0) * (x - x0)
		// y - f(x0) / derivative(x0) = x - x0.
		// (y - f(x0) / derivative(x0)) - x0 = x.
		// that gives us our next iteration right.

		// or if we rewrite it, our next iteration is
		// x(i+1) = x(i) - (f(xi) / f'(fxi)) 
		// where x is relabelled as x(i+1) for our next
		// iteration, and x0 is the current iteration, aka
		// xi.

		// doing this several times allows us to approach 
		// the root of our function f.

		var solution = this.next(x);
		if (null === solution) {
			console.log("Couldn't find a solution in time");
		}
		return solution;
	}

	next(xi) {
		// p137
		// solving for our next iteration using 
		// x(i+1) = x(i) - (f(xi) / f'(fxi)) 
		// where x is relabelled as x(i+1) for our next
		// iteration, and x0 is the current iteration, aka
		// xi.
		this.maxIterations--;

		if (this.maxIterations < 0) {
			// Couldn't find a solution time.
			return null;
		}

		var fx = this.f(xi);

		if (0 === fx) {
			return xi;
		}

		var ffx = this.derivative.f(xi);
		var n = xi - (fx/ffx);

		return this.next(n);
	}
}