class Box {
	// p143
	// they describe a box as made up of six planar equations
	// where:
	// x = 0, x = rx
	// y = 0, y = ry
	// z = 0, z = rz
	// where rx, ry, rz are the dimensions of the box.
	// a planar equation where x = 0 (meaning coefficeints of 
	// y, and z are 0) is just a plane expanding in infinite 
	// directions perpendicular to the y axis (aka the normal
	// should be 0, 1, 0).
	// i guess this means the box must be based at the origin?
	// and be axis-aligned. modifying 'd' in the planar equation
	// shifts the plane.
	// http://www.songho.ca/math/plane/plane.html
	// we'll get the rx, ry and rz as the values from
	// dimensions. we'll give those planes the same normals
	// as our x, y, and z planes but move their point away
	// based on the dimension.
	constructor(dimensions) {
		Vec3._check(dimensions);
		this.dimensions = dimensions;
	}

	get x() {
		if (!this._x) {
			this._x = Plane.plane([1, 0, 0], [0, 0, 0]);
		}
		return this._x;
	}

	get y() {
		if (!this._y) {
			this._y = Plane.plane([0, 1, 0], [0, 0, 0]);
		}
		return this._y;
	}

	get z() {
		if (!this._z) {
			this._z = Plane.plane([0, 0, 1], [0, 0, 0]);
		}
		return this._z;
	}

	get rx() {
		if (!this._rx) {
			this._rx = Plane.plane([1, 0, 0], 
				[this.dimensions[0], 0, 0]);
		}
		return this._rx;
	}

	get ry() {
		if (!this._ry) {
			this._ry = Plane.plane([0, 1, 0], 
				[0, this.dimensions[1], 0]);
		}
		return this._ry;
	}

	get rz() {
		if (!this._rz) {
			this._rz = Plane.plane([0, 0, 1], 
				[0, 0, this.dimensions[2]]);
		}
		return this._rz;
	}

	rayIntersection(ray) {
		// p143
		// we can apply optimizations based on the ray's 
		// direction V. this lets us decide which planes
		// should be tested. i think the optimization here
		// assumes that the ray never starts inside of the
		// box, otherwise the results get weird.
		var { x, y, z, rx, ry, rz } = this;

		var hitPoint = this.hitPointForBoundsGroup(
			[x, y, z], ray, function (v) { return (v < 0 || 0 === v); }
		);

		if (hitPoint) {
			return hitPoint;
		}

		hitPoint = this.hitPointForBoundsGroup(
			[rx, ry, rz], ray, function (v) { return (v > 0 || 0 === v); }
		);

		return hitPoint;
	}

	hitPointForBoundsGroup(group, ray, inequalityFunc) {
		var { s, v } = ray;

		for (var j = 0; j < group.length; j++) {
			var plane = group[j];
			var hit = true;

			if (inequalityFunc(v[j])) {
				continue;
			}
			var point = plane.rayIntersection(ray);
			if (!point) {
				continue;
			}
			// a dirty way of getting the other two planes. so if we're doing
			// x, we should get [1, 2]. if we're doing y, we'll get [0, 2].
			var correspondingIndices = [0, 1, 2].filter(d => d != j);

			var t = (this.dimensions[j] - s[j]) / v[j];
			var pt = Vec3.add(s, Vec3.multiply(v, t));

			for (var i = 0; i < correspondingIndices.length; i++) {
				// check that it falls within the other dimensions.
				var index = correspondingIndices[i];
				hit = this.dimensions[index] >= pt[index] && pt[index] >= 0;
				if (!hit) {
					break;
				}
			}
			if (hit) {
				return point;
			}
		}
		return null;
	}
}