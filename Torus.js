class Torus {
	// p147
	// a torus is made up of primary radius r1, and
	// secondary radius r2. r2 is the radius of the circle
	// that rings around the torus, like along a spline.
	// r1 is the radius of that spline that r2 runs
	// around. r1 can be though of a circle lying
	// on the x-z plane (book uses z as up). we can think
	// of r1 and r2 being perpendicular to one another,
	// with r2 revolving around the y-axis (book says
	// around z, but we're treating z as forward, not up).
	//
	// we can describe r2 as s^2 + y^2 = r2^2
	// where s^2 is the distance to the primary circle in
	// the x-z plane
	// s = sqrt(x^2 + z^2) - r1
	//
	// after some substitution and expansion, we get
	// this as the equation of the torus:
	//
	// (x^2 + y^2 + z^2 + r1^2 + r2^2)^2 = 4 * r1^2 * (x^2 + z^2)
	//
	// we would then substitute our ray to find a point
	// that would solve the equation.
	// once again, this is assuming that the torus is centred
	// around the origin.
	constructor(r1, r2) {
		this.r1 = r1;
		this.r2 = r2;
	}

	rayIntersection(ray) {
		// we have between 0 and 4 points of intersection
		// in the case where there is more than 1 point
		// we return the one closest to where our ray began.
		// we might have to rework this if the torus is
		// rotated.
		var { r1, r2 } = this;
		var { s, v } = ray;

		var rr1 = r1 * r1;
		var rr2 = r2 * r2;
		var vv = Vec3.dot(v, v);
		var sv = Vec3.dot(s, v);
		var ss = Vec3.dot(s, s);

		// p146
		// as we said above, the y-axis is forward in the
		// diagram, whereas we know the z-axis is forward
		// in our case.
		var a = vv * vv;
		var b = 4 * vv * sv;

		var c1 = 2 * vv * (ss + rr1 - rr2);
		var c2 = 4 * rr1 * (v[0] * v[0] + v[2] * v[2]);
		var c3 = 4 * sv * sv;
		var c = c1 - c2 + c3;

		var d1 = 8 * rr1 * s[1] * v[1];
		var d2 = 4 * sv * (ss - rr1 - rr2);
		var d = d1 + d2;

		var sxx = s[0] * s[0];
		var syy = s[1] * s[1];
		var szz = s[2] * s[2];

		var e1 = sxx * sxx + syy * syy;
		var e2 = szz * szz + (rr1 - rr2) * (rr1 - rr2);
		var e3 = sxx * szz + syy * (rr1 - rr2);
		var e4 = (sxx + szz) * (syy - rr1 - rr2);
		var e = e1 + e2 + 2 * (e3 + e4);

		// our quartic divides by a to get a leading coefficient
		// of 1, so we don't need to do that here.
		// a quartic makes sense because there could be 4 points
		// of intersection, so 4 points could be on the surface
		// of the torus as the ray goes through it.
		var quartic = new PolynomialQuartic(a, b, c, d, e);
		var t = quartic.t;

		if (!t.length) {
			return null;
		}
		// we now need to plug it back into our line equation
		// to establish the point that the intersection happens
		// otherwise we just have a scalar.
		// we might have up to 4 points of intersection.
		var closestPoint = null;
		var d = Infinity;
		for (var i = 0; i < t.length; i++) {
			// try out different solutions and see which one
			// is closer to the starting point 's' of our ray
			// TODO: this isn't quite cutting it, for example
			// a ray starting at 0, 0, 0 with a direction of
			// 0, 0, 1 might think it's hitting the negative
			// side before the positive side.
			var ti = t[i];
			var point = Vec3.add(s, Vec3.multiply(v, ti));
			var m = Vec3.magnitude(Vec3.subtract(point, s));
			if (m < d) {
				d = m;
				closestPoint = point;
			}
		}
		return closestPoint;
	}
}