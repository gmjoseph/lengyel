class Vec2 extends Vec {

	static multiplyMat2(v, m) {
		if ((m.length | m[0].length | v.length | 2) !== 2) {
			throw new Error(`mismatched vec2 length ${v.length} ${m.length}`);
		}
		return [
			v[0] * m[0][0] + v[1] * m[0][1],
			v[0] * m[1][0] + v[1] * m[1][1]
		];
	}

}