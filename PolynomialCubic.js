class PolynomialCubic {
	// https://en.wikipedia.org/wiki/Cubic_function
	constructor(a, b, c, d) {
		// we have 4 values, but a good shortcut is to always
		// let a = 1, so we can skip all the operations on a.
		// lengyel's book takes that approach but i'll be following
		// the steps from the wikipedia article.
		// til a cubic polynomial where the coefficient of the cubed
		// term is 'monic'.
		// we force 'a' (the coefficient of the cubed term) to be
		// monic so we don't have to do as many calculations.
		this.a = a/a;
		this.b = b/a;
		this.c = c/a;
		this.d = d/a;
	}

	get discriminant() {
		if (!this._discriminant) {
			var { b, c, d } = this;
			var v = 18 * b * c * d;
			var w = 4 * (b * b * b) * d;
			var x = (b * b) * (c * c);
			var y = 4 * (c * c * c);
			var z = 27 * (d * d);
			this._discriminant = (v - w + x - y - z);
		}
		return this._discriminant;
	}

	get deltaZero() {
		if (!this._deltaZero) {
			var { b, c } = this;
			var x = b * b;
			var y = 3 * c;
			this._deltaZero = (x - y);
		}
		return this._deltaZero;
	}

	get deltaOne() {
		if (!this._deltaOne) {
			var { b, c, d } = this;
			var x = 2 * (b * b * b);
			var y = 9 * b * c;
			var z = 27 * d;
			this._deltaOne = (x - y + z);
		}
		return this._deltaOne;
	}

	get bigC() {
		// "There are three possible cube roots implied by the expression, 
		// of which at least two are non-real complex numbers; 
		// any of these may be chosen when defining C."
		if (!this._bigC) {
			var d0 = this.deltaZero;
			var d1 = this.deltaOne;

			var x = d1 * d1;
			var y = 4 * (d0 * d0 * d0);
			var z = Math.sqrt((x - y));

			// var w1 = (d1 + z) / 2;
			// var w2 = (d1 - z) / 2;

			// var c1 = Math.cbrt(w1);
			// var c2 = Math.cbrt(w2);

			// which is the imaginary? don't pick the imaginary...
			// on second thought, z would be imaginary if y > x
			// and then c1 or c2 would be imaginary...
			// this._bigC = isNaN(c1) ? c2 : c1;

			var w = (d1 + z) / 2;
			this._bigC = Math.cbrt(w);
		}
		return this._bigC;
	}

	// the number of roots this cubic has.
	get realRootsCount() {
		var d = this.discriminant;
		if (d > 0) {
			return 3;
		} else if (0 === d) {
			return 2;
		} else if (d < 0) {
			return 1
		}
	}

	get theta() {
		return 0;
	}

	x1() {
		// one real root, x1.
		// https://en.wikipedia.org/wiki/Cubic_function#General_formula
		var { b } = this;
		var d0 = this.deltaZero;
		var C = this.bigC;

		var x = d0 / C;
		var y = -1 / 3;
		var z = (b + C + x);

		return (y * z);
	}

	x1x2() {
		// two real roots, x1 x2.
		// https://en.wikipedia.org/wiki/Cubic_function#Multiple_roots,_%CE%94_=_0
		var { b, c, d } = this;
		var d0 = this.deltaZero;

		var s = 9 * d;
		var t = b * c;
		var u = 2 * d0;
		// the double root
		var x1 = (s - t) / u;

		var v = 4 * b * c;
		var w = 9 * d;
		var x = b * b * b;
		var y = v - w - x;
		// the simple root
		var x2 = (y / d0);

		return [x1, x2];
	}


	x1x2x3() {
		// three real roots, x1, x2 and x2
		// https://en.wikipedia.org/wiki/Cubic_function#Trigonometric_solution_for_three_real_roots
		var { b, c, d } = this;

		var p = (3 * c - b * b) / 3;
		var q = this.deltaOne / 27;

		var tK = function(p, q, kN) {
			// "This formula involves only real terms if p < 0 
			// and the argument of the arccosine is between −1 and 1."
			var k = (Math.PI * 2 / 3) * kN;

			var f0 = (((3 * q) / (2 * p)) * Math.sqrt(-3 / p));
			var f1 = (1 / 3) * Math.acos(f0);
			var f2 = f1 - k;
			var f3 = Math.cos(f2);
			var f4 = 2 * Math.sqrt((-p / 3));

			var x = f4 * f3;
			return x;
		}

		// var x1 = tK(p, q, 0) - b / (3 * a);
		// var x2 = tK(p, q, 1) - b / (3 * a);
		// var x3 = tK(p, q, 2) - b / (3 * a);

		// a nice simplification by expressing the other two real roots as
		// functions of k = 0, applying modifications to p/q.
		// shown towards the bottom of this section: 
		// https://en.wikipedia.org/wiki/Cubic_function#Trigonometric_solution_for_three_real_roots
		var x1 = tK(p, q, 0);
		var x3 = tK(p, -q, 0) * -1;
		var x2 = -x1 - x3;

		// what comes into play next is that we've reduced to a 'depressed cubic'
		// https://en.wikipedia.org/wiki/Cubic_function#Reduction_to_a_depressed_cubic
		// if the coefficient of the quadratic wasn't already 0 (i.e. we didn't
		// start with a depressed cubic from the outset), then we need to solve 
		// for our x values by doing:
		// xk = t - b/(3 * a)
		// this assumes that the trinomial is monic (i.e. 1 is the coefficient
		// of the cubic term, aka a = 1).
		if (0 !== b && 1 === this.a) {
			var subtrahend = b / 3;
			x1 = x1 - subtrahend;
			x2 = x2 - subtrahend;
			x3 = x3 - subtrahend; 
		}

		// these should all be 0.
		// console.log(x1 * x1 * x1 + p * x1  + q);
		// console.log(x2 * x2 * x2 + p * x2  + q);
		// console.log(x3 * x3 * x3 + p * x3  + q);

		return [x1, x2, x3];
	}

	get t() {
		if (!this._t) {
			var rrc = this.realRootsCount;
			if (1 === rrc) {
				this._t = this.x1();
			} else if (2 === rrc) {
				this._t = this.x1x2();
			} else if (3 === rrc) {
				this._t = this.x1x2x3();
			}
		}
		return this._t;
	}

	test() {
		var { b, c, d } = this;
		var solutions = this.t;

		if (!solutions.length) {
			solutions = [solutions];
		}

		var passed = true;
		for (var i = 0; i < solutions.length; i++) {
			var t = solutions[i];
			var w = t * t * t;
			var x = t * t 	  * b;
			var y = t 		  * c;
			var z = 1 		  * d;
			var f = w + x + y + z;
			if (0 !== Math.round(f)) {
				passed = false;
				break;
			}
		}

		return `test passed: ${passed}`;
	}
}