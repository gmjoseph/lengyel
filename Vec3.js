class Vec3 extends Vec {
	static _check(v) {
		if ((v.length | v[0].length | 3) !== 3) {
			throw new Error(`mismatched vec3 length ${v.length}`);
		}
	}

	static _checkBoth(v1, v2) {
		Vec3._check(v1);
		Vec3._check(v2);
	}

	static multiplyMat3(v, m) {
		Vec3._check(v);
		Mat3._check(m);
		// confirm:
		// https://www.symbolab.com/solver/matrix-vector-calculator/
		return [
			v[0] * m[0][0] + v[1] * m[0][1] + v[2] * m[0][2],
			v[0] * m[1][0] + v[1] * m[1][1] + v[2] * m[1][2],
			v[0] * m[2][0] + v[1] * m[2][1] + v[2] * m[2][2],
		];
	}

	// at some point we'll probably wanna switch over to 
	// mat4 and vec4 due to this. plus the book covers it
	// in depth and makes use of it:
	// https://gamedev.stackexchange.com/questions/68891/why-translation-uses-multiplication-and-not-addition
	static translate(v1, v2) {
		return Vec3.add(v1, v2);
	}

	static scale(v, m) {
		return Vec3.multiplyMat3(v, m);
	}

	static rotateX(v, a) {
		return Vec3.multiplyMat3(
			v,
			[
				[1, 0, 0],
				[0, Math.cos(a), -Math.sin(a)],
				[0, Math.sin(a), Math.cos(a)],
			]
		);
	}

	static rotateY(v, a) {
		return Vec3.multiplyMat3(
			v,
			[
				[Math.cos(a), 0, Math.sin(a)],
				[0, 1, 0],
				[Math.sin(a), 0, Math.cos(a)],
			]
		);

	}

	static rotateZ(v, a) {
		return Vec3.multiplyMat3(
			v,
			[
				[Math.cos(a), -Math.sin(a), 0],
				[Math.sin(a), Math.cos(a), 0],
				[0, 0, 1],
			]
		);
	}

	static rotateAxis(v, axis, a) {
		// https://stackoverflow.com/a/6721649
		var cos = Math.cos(a);
		var sin = Math.sin(a);
		var n = Vec3.normalize(axis);
		// creates the multiplication matrix, we still need
		// to multiply our vector v1 by it.
		var r = [
			[
				cos + n[0] * n[0] * (1 - cos),
				n[0] * n[1] * (1 - cos) - n[2] * sin,
				n[0] * n[2] * (1 - cos) + n[1] * sin,
			],
			[
				n[1] * n[0] * (1 - cos)  + n[2] * sin,
				cos + n[1] * n[1] * (1 - cos),
				n[1] * n[2] * (1 - cos) - n[0] * sin,
			],
			[
				n[2] * n[0] * (1 - cos) - n[1] * sin,
				n[2] * n[1] * (1 - cos) + n[0] * sin,
				cos + n[2] * n[2] * (1 - cos),
			],
		];
		return Vec3.multiplyMat3(v, r);
	}
}