class Quat {
	// length can be a function on Quat.
	static _check(q) {
		if ((q.length | q[0].length | 4) !== 4) {
			throw new Error(`mismatched quat length ${q.length}`);
		}
		// not sure this is required, also it'll mess up
		// any vectors we try and multiply when we multiply
		// un-normalized vectors by a quaternion.
		// for (var i = 0; i < q.length; i++) {
		// 	if (q[i] > 1 || q[1] < -1) {
		// 		throw new Error(`quat must be normalized`);
		// 	}
		// }
	}

	static _checkBoth(q1, q2) {
		Quat._check(q1);
		Quat._check(q2);
	}

	static add(q1, q2) {
		Quat._checkBoth(q1, q2);
		return q1.map((d, i) => (d + q2[i]));
	}

	static multiply(q1, q2) {
		Quat._checkBoth(q1, q2);
		return [
			(q1[3] * q2[0] + q1[0] * q2[3] + q1[1] * q2[2] - q1[2] * q2[1]),
	        (q1[3] * q2[1] - q1[0] * q2[2] + q1[1] * q2[3] + q1[2] * q2[0]),
	        (q1[3] * q2[2] + q1[0] * q2[1] - q1[1] * q2[0] + q1[2] * q2[3]),
	        (q1[3] * q2[3] - q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2])
		];
	}

	static conjugate(q) {
		Quat._check(q);
		return [-q[0], -q[1], -q[2], q[3]];
	}

	static normalize(q) {
		Quat._check(q);
		var n = Math.sqrt(
			q.reduce(((acc, d) => (acc += d * d)), 0)
		);
		return q.map(d => (d / n));
	}

	static rotation(axis, a) {
		// gives us the rotation quaternion around
		// the axis for angle a
		// https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Using_quaternion_rotations
		var n = Vec3.normalize(axis);
		var w = Math.cos(a * 0.5)
		var sin = Math.sin(a * 0.5);
		var x = sin * n[0];
		var y = sin * n[1];
		var z = sin * n[2];
		return [x, y, z, w];
	}

	static angleFromQuat(q) {
		// might be better:
		// https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Recovering_the_axis-angle_representation
		Quat._check(q);
		var w = q[3];
		return Math.acos(w) * 2;
	}

	static axisFromQuat(q) {
		// might be better:
		// https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Recovering_the_axis-angle_representation
		Quat._check(q);
		var a = Quat.angleFromQuat(q);
		var sin = Math.sin(a * 0.5);
		return [q[0] / sin, q[1] / sin, q[2] / sin];
	}

	static equal(q1, q2) {
		Quat._checkBoth(q1, q2);
		for (var i = 0; i < q1.length; i++) {
			if (!(q1[i], q2[i])) {
				return false;
			}
		}
		return true;
	}

	static rotateAxis(v, axis, a) {
		// the vector we rotate.
		var vec = v.map(d => d);
		// real coordinate = 0. the imaginary
		// components have some other value.
		vec.push(0);
		var r = Quat.rotation(axis, a);
		// now we've got a rotation quaternion and a vector
		// to rotate.
		var vecPrime = Quat.multiply(
			Quat.multiply(r, vec),
			Quat.conjugate(r)
		);

		return [vecPrime[0],vecPrime[1], vecPrime[2]] 
	}

	static slerp(q1, q2, t) {
		Quat._checkBoth(q1, q2);
		if (t < 0 || t > 1) {
			throw new Error(`slerp requires t to be between 0 and 1`);
		}

		var nq1 = q1.map(a => ((1 - t) * a));
		var nq2 = q2.map(a => t * a);
		var qd = Quat.add(nq1, nq2);
		var n = Quat.normalize(qd);
		var magnitude = Math.sqrt(
			n.reduce(((acc, d) => (acc += d * d)), 0)
		);
		return qd.map(a => a / magnitude);
	}
}