class Plane {
	// p97
	// 1 point P known to lie on the plane, 1 normal N
	// and 1 value Q where Q-P is perpendicular to the 
	// normal N. 
	// since a dot product of 0 means two vectors are
	// perpendicular, we can say dot(N, Q-P) = 0 defines
	// our plane.

	static plane(n, p) {
		Vec3._check(n);
		Vec3._check(p);
		var plane = new Plane();
		plane.normal = Vec3.normalize(n);
		plane.point = p;
		// if (Vec3.dot(plane.normal, p-q?)) {
		// 	throw new Error(`a plane is defined by two perpendicular vectors`);
		// }
		if (0 != Vec3.dot(plane.normal, p) + plane.D) {
			throw new Error(`n dot p + D must be equal to 0`);
		}
		return plane;
	}

	get D() {
		return Vec3.dot(Vec3.multiply(this.normal, -1), this.point);
	}

	distanceToPoint(q) {
		Vec3._check(q);
		var D = this.D;
		var d = Vec3.dot(this.normal, q) + D;
		return d;
	}

	pointIsOnPlane(q) {
		return 0 === this.distanceToPoint(q);
	}

	pointIsOnNegativeSide(q) {
		return 0 > this.distanceToPoint(q);
	}

	pointIsOnPositiveSide(q) {
		return 0 < this.distanceToPoint(q);
	}

	offsetFromOrigin() {
		var D = this.D;
		var n = Vec3.magnitude(this.normal);
		return Math.abs(D) / n;
	}

	rayIntersection(ray) {
		// p99
		// we're solving: N dot P(t) + D = 0
		// P(t) is just S + tV, while D is -N dot P
		// so we distribute N dot to get 
		// N dot S + (N dot V)t + D = 0
		// D, S, N, and V are all known to us, t is
		// our unknown, so we'll rearrange the equation
		// to solve for t:
		// t = -(N dot S + D) / N dot V. 
		// if we get t we can figure out where along the
		// ray the point of intersection is.
		// if N dot V is 0, the ray is parallel to the plane
		// since N dot someValue = 0 defines our plane,
		// we know N dot V means N and V are perpendicular
		// meaning that V can never intersect the plane.
		// we can build off that further, if N dot S + D is
		// 0, then we know that the ray is in the plane 
		// itself.

		// confirm using ray defined by vector and plane equation.
		// http://www.ambrsoft.com/TrigoCalc/Plan3D/PlaneLineIntersection_.htm
		var D = this.D;
		var NdotS = Vec3.dot(this.normal, ray.s);
		var NdotV = Vec3.dot(this.normal, ray.v);
		if (0 === NdotV) {
			// parallel.
			return null;
		}
		if (0 === NdotS + D) {
			// ray lies within the plane.
			return null;
		}
		var t = -(NdotS + D) / NdotV;

		return Vec3.add(ray.s, Vec3.multiply(ray.v, t));
	}

	static rayPlaneIntersection(ray, plane) {
		return plane.rayIntersection(ray);
	}

	planePlaneIntersection(plane1, plane2) {
		// p99
		// we're taking the normal (Vec3) of the plane
		// and making it into a Vec4 by adding D as the 
		// 'w' (4th) value of the vector, while the normal
		// N makes up the x, y, z values. this new vector
		// is L (Vec4).
		// we know the 3 planes intersect where: 
		// L(plane1) dot Q = 0
		// L(plane2) dot Q = 0
		// L(plane3) dot Q = 0

		// p100
		// we rewrite this as MQ = [-D1, -D2, -D3]
		// where M is 
		//  [N1x N1y N1z]
		//  [N2x N2y N2z]
		//  [N3x N3y N3z]
		// so to solve for Q just multiply the D vector
		// by M^-1 (the inverse of M)

		// confirm: http://www.wolframalpha.com/widgets/view.jsp?id=9698edf1068560b9547bf78b31eecca1
		// we have Ax + By + Cz + D = 0, we subtract D from both sides
		// and set our planar equations equal to D.

		var D = [-this.D, -plane1.D, -plane2.D];
		var M = [
			this.normal,
			plane1.normal,
			plane2.normal,
		];

		var inverse = Mat3.inverse(M);
		if (0 === Mat3.determinant(inverse)) {
			// planes don't intersect at a single point.
			return null;
		}
		// our point of intersection.
		var Q = Vec3.multiplyMat3(D, inverse);
		return Q;
	}

	planeIntersection(plane1) {
		// p101 
		// two non-parallel planes intersect at a line.
		// the line of intersection, V, is perpendicular
		// to both plane's normals. that is, V is the
		// cross product of both plane's normals since
		// it must be simultaneously perpendicular to 
		// both normals, which is what is produced by the
		// cross product.
		// since we know the direction of the line where
		// the planes intersect (it's the cross product
		// of their normals, after all), we just need a
		// starting point to complete the description of
		// the line. 
		// we do this by making a third plane that passes 
		// through the origin (D = 0) and has the cross
		// product V as its normal. we then apply the
		// same procedure as the 3 plane intersection to
		// get point Q.
		// our line of intersection is then P(t) = Q + tV.

		// confirm: http://www.ambrsoft.com/TrigoCalc/Plan3D/Plane3D_.htm

		var V = Vec3.cross(this.normal, plane1.normal);
		var plane2 = Plane.plane(V, [0, 0, 0]);
		var Q = this.planePlaneIntersection(plane1, plane2);

		return Line.ray(Q, V);
	}
	
	transform(m) {
		throw new Error(`not implemented`);
		// p102
		// we need to move into R4 for this if we're using
		// a 3x3 transformation matrix AND a translation
		// vector T. basically we're combining the rotation
		// and translation transforms. skipped for now,
		// handling the translate and the rotation separately.
	}

	translate(v) {
		// translating the plane should NOT modify
		// the direction of the normal.
		Vec3._check(v);
		this.point = Vec3.add(v, this.point);
	}

	rotateAxis(axis, angle) {
		// rotation around an arbitrary axis can be
		// done using a quaternion. we'll rotate both
		// the point that lies on the plane, as well
		// as its normal.
		this.point = Quat.rotateAxis(this.point, axis, angle);
		this.normal = Quat.rotateAxis(this.normal, axis, angle);
	}
}