class Mat2 {
	// length can be a function on Mat2.
	static _check(m) {
		if ((m.length | m[0].length | 2) !== 2) {
			throw new Error(`mismatched mat2 length ${m.length}`);
		}
	}

	static _checkBoth(m1, m2) {
		Mat2._check(m1);
		Mat2._check(m2);
	}

	static add(m1, m2) {
		Mat2._checkBoth(m1, m2);
		return m1.map((v, i) => Vec2.add(v, m2[i]));
	}

	static subtract(m1, m2) {
		Mat2._checkBoth(m1, m2);
		return Mat2.add(m1, Mat2.multiplyScalar(m2, -1));
	}

	static multiply(m1, m2) {
		Mat2._checkBoth(m1, m2);
		return [
			[
				m1[0][0] * m2[0][0] + m1[0][1] * m2[1][0],
				m1[0][0] * m2[0][1] + m1[0][1] * m2[1][1],
			],
			[
				m1[1][0] * m2[0][0] + m1[1][1] * m2[1][0],
				m1[1][0] * m2[0][1] + m1[1][1] * m2[1][1],
			]
		];
	}

	static multiplyScalar(m, scalar) {
		Mat2._check(m);
		return m.map(v => Vec2.multiply(v, scalar));
	}

	static inverse(m) {
		Mat2._check(m);
		const d = Mat2.determinant(m);
		if (0 === d) {
			return null;
		}
		return (
			Mat2.multiplyScalar(
				[
					[m[1][1], -m[0][1]],
					[-m[1][0], m[0][0]]
				],
				1 / d
			)
		);
	}

	static determinant(m) {
		Mat2._check(m);
		return (
			m[0][0] * m[1][1] - m[0][1] * m[1][0]
		);
	}

	static transpose(m) {
		Mat2._check(m);
		return [
			[m[0][0], m[1][0]],
			[m[0][1], m[1][1]]
		];
	}

	static equal(m1, m2) {
		Mat2._checkBoth(m1, m2);
		for (var i = 0; i < m1.length; i++) {
			if (!(Vec2.equal(m1[i], m2[i]))) {
				return false;
			}
		}
		return true;
	}
}